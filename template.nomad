job "microbot" {
  region = "nonprod"
  datacenters = ["DC1"]
  # Rolling updates
  #update {
  #  stagger = "10s"
  #  max_parallel = 1
  # }
  group "web" {
    # We want 9 web servers initially
    count = 4
    task "microbot" {
      driver = "docker"
      config {
        image = "registry.gitlab.com/moayad1/nusdemo:image_version"
	auth {
		username = "pandom"
		password = "place_password_here"
     	}
	port_map {
          http = 80
        }
      }
      # service {
      #  port = "http"
      #  check {
      #    type = "http"
      #    path = "/"
      #    interval = "10s"
      #    timeout = "2s"
      #  }
      # }
      env {
        DEMO_NAME = "nomad-intro"
      }
      resources {
        cpu = 500
        memory = 256
        network {
          mbits = 100
          port "http" {}
        }
      }
    }
  }
}