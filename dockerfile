FROM frolvlad/alpine-glibc
ENV NOMAD_VERSION 0.11.1
ENV NOMAD_RELEASE https://releases.hashicorp.com/nomad/$NOMAD_VERSION/nomad_${NOMAD_VERSION}_linux_amd64.zip
# ca-certificates is needed to verify releases.hashicorp.com’s certificate
RUN apk add --update bash wget gettext tar ca-certificates
RUN wget ${NOMAD_RELEASE} -O nomad.zip -o /dev/null
RUN cd /usr/local/bin \
  && unzip /nomad.zip \
  && nomad --version
RUN apk del wget tar ca-certificates \
  && rm -rf /var/cache/apk/*